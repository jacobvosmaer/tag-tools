#include <string.h>
#include <iostream>
#include <json/writer.h>
#include <taglib/mp4file.h>

int main(int argc, char **argv) {
  if (argc != 2) {
    std::cout << "usage: " << argv[0] << " file.m4a" << std::endl;
    exit(1);
  }

  const char *filename = argv[1];
  std::string line;
  bool multiple_files = strcmp(argv[1], "-") == 0;
  while(1) {
    if (multiple_files) {
      if (!std::getline(std::cin, line)) {
        break;
      }
      filename = line.c_str();
    }

    TagLib::MP4::File file(filename);
    TagLib::MP4::Tag *tag = file.tag();
    if (!file.isValid() || tag->isEmpty()) {
      std::cerr << "Invalid MP4 file: " << filename << std::endl;
      exit(1);
    }
  
    Json::Value md;
    md["length"] = file.audioProperties()->length();
    md["title"] = tag->title().toCString(true);
    md["artist"] = tag->artist().toCString(true);
    md["year"] = tag->year();
    md["genre"] = tag->genre().toCString(true);
  
    TagLib::MP4::ItemListMap &itmap = tag->itemListMap();
    TagLib::MP4::ItemListMap::ConstIterator iter = itmap.find("aART");
    if (iter != itmap.end()) {
      TagLib::StringList sl = iter->second.toStringList();
      md["album_artist"] = sl.toString().toCString(true);
    }
  
    std::cout << md << std::endl;
    if (!multiple_files) {
      break;
    }
  }
}
