#include <iostream>
#include <taglib/mp4file.h>

int main(int argc, char **argv) {
  if (argc < 3 || (argc % 2) != 1) {
    std::cout << "usage: " << argv[0] << " KEY VALUE [KEY VALUE] ..."
              << std::endl;
    exit(1);
  }

  int nArgs = (argc - 1) / 2;
  std::vector< std::pair< TagLib::String, TagLib::MP4::Item > > mp4Args(nArgs);
  for (int i = 1; i < argc; i += 2) {
    // Apparently keys like \xa9nam are not valid UTF8 but they work as Latin1
    TagLib::String key(argv[i]);
    TagLib::String value_string(argv[i + 1], TagLib::String::UTF8);
    TagLib::MP4::Item item(value_string);
    mp4Args[(i-1) / 2] = std::make_pair(key, item);
  }

  std::string line;
  while (std::getline(std::cin, line)) {
    TagLib::MP4::File file(line.c_str());
    if (!file.isValid()) {
      std::cerr << "Invalid MP4 file: " << line << std::endl;
      exit(1);
    }

    // If we don't use a pointer C++ lets us modify a free-floating
    // ItemListMap instance that does not get saved in the file.
    TagLib::MP4::ItemListMap *iMap = &(file.tag()->itemListMap());
    for (int i = 0; i < nArgs; ++i) {
      iMap->insert(mp4Args[i].first,  mp4Args[i].second);
    }

    if (!file.save()) {
      std::cerr << "Aborting: failed to save " << line << std::endl;
      exit(1);
    }
  }
}
