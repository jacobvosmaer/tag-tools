PREFIX=/usr/local
LDLIBS=-ljsoncpp -ltag

all:	tag-dump flac-set m4a-set flac-cp-tag

install:	tag-dump flac-set m4a-set flac-cp-tag
	install $^ ${PREFIX}/bin/

test: tag-dump
	./tag-dump testdata/test.m4a | ruby test.rb
	@echo SUCCESS

.PHONY:	clean
clean:
	rm -f tag-dump flac-set m4a-set flac-cp-tag
