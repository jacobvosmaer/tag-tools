require 'json'

def assert(value, expected)
  if value != expected
    raise "Expected #{value.inspect} to equal #{expected.inspect}"
  end
end

def test
  json = JSON.parse(ARGF.read)

  assert(json.delete('title'), 'Title')
  assert(json.delete('artist'), 'Artist')
  assert(json.delete('album_artist'), 'Album Artist')
  assert(json.delete('genre'), 'Genre')
  assert(json.delete('length'), 1)
  assert(json.delete('year'), 1234)
  assert(json, {})
end

test