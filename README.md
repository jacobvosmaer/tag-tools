# tag tools

This software was written for personal use by Jacob Vosmaer. I am
sharing it as an example only.

## Installation

```
brew install jsoncpp taglib
make install PREFIX=foo
```

You may need to point the way to where jsoncpp and taglib are installed.

```
make LDFLAGS=-I/opt/local/include CPPFLAGS=-L/opt/local/lib
```

## Test

```
make test
```

## License

See the LICENSE file.

## Bugs

Some of this code does not compile with GCC. Use Clang (`make CXX=clang++`).