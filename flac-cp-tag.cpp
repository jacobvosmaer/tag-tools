#include <iostream>
#include <taglib/flacfile.h>
#include <taglib/xiphcomment.h>
#include <taglib/tpropertymap.h>

int main(int argc, char **argv) {
  if (argc != 3) {
    std::cout << "usage: " << argv[0] << " SOURCE_FLAC DEST_FLAC" << std::endl;
    exit(1);
  }
  
  TagLib::FLAC::File src(argv[1]);
  if (!src.isValid()) {
    std::cerr << "Invalid FLAC file: " << argv[1] << std::endl;
    exit(1);
  }

  TagLib::FLAC::File dst(argv[2]);
  if (!dst.isValid()) {
    std::cerr << "Invalid FLAC file: " << argv[2] << std::endl;
    exit(1);
  }

  dst.xiphComment()->setProperties(src.xiphComment()->properties());

  if (!dst.save()) {
    std::cerr << "Aborting: failed to save " << argv[2] << std::endl;
    exit(1);
  }
}
