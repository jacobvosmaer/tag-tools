#include <iostream>
#include <taglib/flacfile.h>
#include <taglib/xiphcomment.h>

int main(int argc, char **argv) {
  if (argc < 3 || (argc % 2) != 1) {
    std::cout << "usage: " << argv[0] << " KEY VALUE [KEY VALUE] ..."
              << std::endl;
    exit(1);
  }
  int nArgs = argc - 1;
  std::vector<TagLib::String> xiphArgs(nArgs);
  for (int i = 1; i < argc; ++i) {
    xiphArgs[i-1] = TagLib::String::String(argv[i], TagLib::String::UTF8);
  }

  std::string line;
  while (std::getline(std::cin, line)) {
    TagLib::FLAC::File file(line.c_str());
    if (!file.isValid()) {
      std::cerr << "Invalid FLAC file: " << line << std::endl;
      exit(1);
    }
    for (int i = 0; i < nArgs; i += 2) {
      file.xiphComment()->addField(xiphArgs[i], xiphArgs[i + 1]);
    }
    if (!file.save()) {
      std::cerr << "Aborting: failed to save " << line << std::endl;
      exit(1);
    }
  }
}
